﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceComms
{
    [Serializable]
    public class Visiteur // Visiteur (classe) = Visiteur (personne) U Participant
    {
        public int NumBadge { get; set; }
        public string NomComplet { get; set; }
        public string NomSociete { get; set; }
        public string Profession { get; set; }
        public string AutreInfo { get; set; }
    }
}
