﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceComms
{
    [Serializable]
    public class EventSettings
    {
        public DateTime StartEvent { get; set; }
        public DateTime EndEvent { get; set; }
        public int CodeEvent { get; set; }
    }
}
