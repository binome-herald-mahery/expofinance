﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceComms
{
    [Serializable]
    public class VisiteurPassage
    {
        public int DatabaseID { get; set; }
        public DateTime DatePassage { get; set; }
        public int NumBadge { get; set; }
        public string NomComplet { get; set; }
        public string NomSociete { get; set; }
        public string Profession { get; set; }
        public string TypeFlux { get; set; }
    }
}
