﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceComms
{
    [Serializable]
    public class InfoFluxClient
    {
        public string Date { get; set; }
        // La raison du choix de ce type réside dans le fait que l'on n'a pu afficher que la date si cela est du type DateTime
        public string CodeBarre { get; set; }
        public string Nom { get { return this.nom_Personne + " " + this.prenom_Personne; } }
        public string Fonction { get; set; }
        public string TypeEtNuméro { get { return this.typePers + this.indicePers; } }
        public string Pays { get; set; }
        public string TypeDeFlux
        {
            get
            {
                return this._typeFlux;
            }
            set
            {
                if ((value == "E") || (value.ToLower() == "entrée") || (value.ToLower() == "entree"))
                    _typeFlux = "Entrée";
                else if ((value == "S") || (value.ToLower() == "sortie"))
                    _typeFlux = "Sortie";
                else
                    throw new FormatException(
               "Valeur attendue: \"E\", \"S\",\"Entrée\", \"Sortie\", \"entree\", \"sortie\", ou tout ce qui contient ces lettres dans l'ordre"
                                             );
            }
        }
        public string Portail { get; set; }



        [Browsable(false)]
        public string nom_Personne { get; set; }
        [Browsable(false)]
        public string prenom_Personne { get; set; }
        [Browsable(false)]
        public string typePers;
        [Browsable(false)]
        public string indicePers;
        [Browsable(false)]
        public DateTime date { get; set; }

        private string _typeFlux;
    }
}
