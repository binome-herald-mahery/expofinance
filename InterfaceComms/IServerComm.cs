﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceComms
{
    public interface IServerComm
    { 
        IServerComm This{get;}

        bool PasseFiltre(string numCodeBarre_, string typePortail_);
        int NombrePersonne(string typePersonne_, ENombrePersonne cumul_, int niveau_) ;
        // typePersonne_ : "TOUS" ; "VIP" ; "VISITEUR" ; "PARTICIPANT" ; + pour les futurs implémentations...
        // niveau_       : Il se peut qu'il y ait plusieurs niveaux de portail (portail interne, externe, etc.)
        //   : Le niveau 0 est celui de l'extérieur
        int NombreTotalParticipant();
        
        InfoFluxClient AjouterVisiteurPassage(int visiteurCode, DateTime dateComplet, char flux, string nomPC, string descriptionPortail, int niveau_);

        bool CodeBarreExiste(string code_);
        bool PeutPasserNiveau(string code_, int niveau_);
        bool PasEncoreValidé(string code_, string flux_, int niveau_);
        bool PeutFaireAction(string flux_, int niveau_);
        bool DejaUtilisé(string code_);
        List<int> NombreFlux(DateTime d);
        List<InfoFluxClient> ListePassage(string nomPC_, int nbMax_);
        




        int NombrePersonne2(string typePersonne_, ENombrePersonne cumul_, int niveau_);
        List<InfoFluxClient> ListePassage2(string nomPC_, int nbMax_, DateTime t1_, DateTime t2_);
        int NombreTotalAutre();
    }
}
