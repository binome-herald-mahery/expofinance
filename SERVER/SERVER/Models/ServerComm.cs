﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq ;
using System.Text;
using System.Threading.Tasks;

using InterfaceComms ;


namespace SERVER.Models
{

    public class ServerComm : MarshalByRefObject, IServerComm
    {
        public delegate void MiseAJourHandler(InfoFlux e);
        public event MiseAJourHandler MiseAJour_Event ;

        public IServerComm This { 
            // Intuitivement je me suis dit qu'on aurait besoin de cela..... et c'est vraiment le cas!!! c'est 
            // ce code qui permet de déserializer l'objet de communication! je n'est rencontré de problème
            // que lorsque j'ai effacé cette ligne de code.
            get
            {
                return this;
            }
        }

        public bool PasseFiltre(string codeBarre_, string typePortail_)
        {
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque;
            var __temp = (from c in __db.CODE_BARREs join p in __db.PERSONNEs on c.IdPers equals p.Id where c.Code.ToString() == codeBarre_ select p.Type).ToList();

            try {
                string __type = __temp[0];
                return resultatTest(__type, typePortail_);
            } catch
            {
                return false;
            }
        }
        private bool resultatTest(string t_, string tP_)
        {
            if (tP_ == "PCP") return true;
            if (tP_ == "RESTO")
            {
                return (t_ == "JOUEUR") || (t_ == "CD") || (t_ == "COACH") ;
            }
            if(tP_ == "CTPART")
            {
                return (t_ == "JOUEUR") || (t_ == "CD") || (t_ =="COACH");
            }
            return false;
        }
        // So de tokn soloina n anaranty methode ty..... tsy n visiteur rehetra n ampidiriny ato fa ze msoratr ao am base de données...
        // Ovao tsy bool tsoun le "isParticipant" io, mety tsy ho PARTICIPANT sy VISITEUR fotsiny n ao manko
        public InfoFluxClient AjouterVisiteurPassage(int visiteurCode, DateTime dateComplet, char flux, string nomPC, string descriptionPortail , int niveau_)
        {
            InfoFluxClient __visiteurPassage = null;
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque;




            string description = "" ;
            if (descriptionPortail == "PCP")
            {
                description = "CARLTON";
            } else if (descriptionPortail == "RESTO")
            {
                description = "RESTAURANT";
            } else if (descriptionPortail == "CTPART")
            {
                description = "E/S des participants";
            }
                
                
                
            FLUX __temporaire = new FLUX()
            {
                Code = visiteurCode.ToString(),
                DateComplet = dateComplet,
                TypeFlux = flux,
                PC_src = nomPC,
                NiveauPortail = niveau_,
                DescriptionPortail = description
            };

            try
            {
                // Rechercher l'ID de la personne titulaire du code barre.
                var __r1 = (
                                from element in __db.CODE_BARREs
                                where (element.Code == visiteurCode.ToString())
                                select element.IdPers
                           ).ToList();

                PERSONNE __personne;

                if (!__r1[0].HasValue) // r1 doit toujours avoir une valeur, mais on fait ceci pour faire "attention"
                {
                    __personne = new PERSONNE() // On n'insère pas celle-ci dans la base de données
                    {
                        Nom = "",
                        Type = "VIP" // Juste comme ça....
                    };
                }
                else
                {
                    // Rechercher les infos correspondant à la personne
                    var __res = (
                                    from element in __db.PERSONNEs
                                    where (element.Id == __r1[0].Value)
                                    select element
                                ).ToList();

                    __personne = __res[0];
                }
                //Insertion...
                BaseDeDonnees.InsererFLUX(__temporaire);

                // Si rien de mal ne s'est passé...
                __visiteurPassage = CréerInfoFluxClient(visiteurCode,dateComplet,flux,__temporaire.DescriptionPortail, __personne);

                // Solution Ad-hoc de dernier moment....
                __personne.Fonction = __visiteurPassage.Fonction; // Pour son éventuel prochain appel...(ci-dessous)

                if (MiseAJour_Event != null)
                    MiseAJour_Event(new InfoFlux(__temporaire, __personne));
            }
            catch
            {
                return null;
            }

            return __visiteurPassage;
        }

        private InfoFluxClient CréerInfoFluxClient(int visiteurCode, DateTime dateComplet, char flux, string descriptionPortail, PERSONNE personne)
        {
            return new InfoFluxClient()
            {
                date = dateComplet,
                Date = dateComplet.ToString(),

                CodeBarre = visiteurCode.ToString(),

                nom_Personne = personne.Nom,

                // Solutions ad-hoc de dernier moment..... à vous de le corriger pour le futur utilisation de ce logiciel..
                Fonction = (((personne.Autre_info != null) && (personne.Autre_info != "") && (personne.Autre_info != "RESTO")) ? (personne.Autre_info) : (personne.Fonction)),

                typePers = (personne.Type == "M_FIPJP" ? "Membre du FIPJP" : personne.Type),

                indicePers = personne.Indice.HasValue ? personne.Indice.Value.ToString() : "",

                Pays =personne.Pays,

                TypeDeFlux = flux.ToString(),

                Portail = descriptionPortail
            };
        }

        public int NombrePersonne(string typePersonne_, ENombrePersonne cumul_, int niveau_)
        {
            DateTime __date = DateTime.Now.Date ;

            // ceux qui se trouvent dans un niveau se trouvent aussi dans tous les niveaux "inférieurs"
            //(une personne se trouvant dans le niveau 1 se trouve aussi dans le niveau 0)
                // Pour l'instant on ne choisit que ces deux types....
            /////////// commentaire pour l'instant: if ((typePersonne_ == "PARTICIPANT") || (typePersonne_ == "PUBLIC") || (typePersonne_ == "JRN"))
            {
                return nombre(typePersonne_, cumul_,niveau_, __date);
            }
            ////////// return 0 ;
        }
        private int nombre(string typePersonne_, ENombrePersonne e_, int niveau_, DateTime __date)
        {
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque;
            var __listeCodeBarreEntree = listeCodeBarreDate(niveau_, __date, 'E'); // Liste des codes barres qui sont entrées.
            var __nbEntree = (
                                    from codeBarre in __listeCodeBarreEntree
                                    join personne in __db.PERSONNEs
                                    on codeBarre.IdPers equals personne.Id
                                    where ((typePersonne_ == "TOUS") || (personne.Type.Trim() == typePersonne_))
                                    select personne.Id
                             ).Count();
            var __listeCodeBarreSortie = listeCodeBarreDate(niveau_, __date, 'S');
            /// Doublon à "supprimer"
            var __nbSortie = (
                                    from codeBarre in __listeCodeBarreSortie
                                    join personne in __db.PERSONNEs
                                    on codeBarre.IdPers equals personne.Id
                                    where ((typePersonne_ == "TOUS") || (personne.Type.Trim() == typePersonne_))
                                    select personne.Id
                                ).Count();
            if (e_ == ENombrePersonne.ACTUEL)
            {
                return __nbEntree - __nbSortie;
            }
            else if (e_==ENombrePersonne.CUMUL_AUJ)
            {
                return __nbEntree;
            }
            // Rah tena ts tafa
            return 0;
        }

        
        private List<CODE_BARRE> listeCodeBarreDate(int niveau_, DateTime __date, char typePortail_)
        {
            return (
                    from code in BaseDeDonnees.NouveauPetanque.FLUXes
                    where (
                        (code.TypeFlux == typePortail_) &&
                        (code.NiveauPortail >= niveau_) &&
                        ((!code.DateComplet.HasValue) || (code.DateComplet.Value.Date == __date.Date))
                    )
                    select code.CODE_BARRE
                ).ToList();
        }
        public bool PasEncoreValidé(string code_, string flux_, int niveau_) // Ok angamba
        {
            int __longueur ;
            var __ListeOccurence = (
                    from c in BaseDeDonnees.NouveauPetanque.FLUXes
                    where ((c.Code == code_) && (c.NiveauPortail == niveau_))
                    select c
                    ).ToList() ;
            __longueur = __ListeOccurence.Count();
            if (__longueur == 0) return (flux_ == "E") ; // Si pas d'occurence et que c'est en mode entrée, alors OK, il peut entrer, sinon il ne peut pas sortir....
            else { return (flux_ == "E"); }
        }

        public bool PeutFaireAction(string flux_, int niveau_) // OK angamba
        {
            try {
                var __valeurMax = (
                        from c in BaseDeDonnees.NouveauPetanque.PARAMETREs
                        where (c.label == ("nb_max_" + niveau_.ToString()))
                        select c.value
                    ).ToList();
                string __valMax = __valeurMax[__valeurMax.Count - 1].ToString();
                int __nbMax = int.Parse(__valMax); // Prendre dans le tableu PARAMETRE la valeur du maximum.

                /// Changer dans le futur...
                int __nbActu = NombrePersonne("TOUS", ENombrePersonne.ACTUEL, niveau_);
                return (__nbMax > __nbActu);
            } catch
            {
                return true; // Si il y a une exception, alors nb_max_NIVEAU n'existe pas dans la base de données...
            }
        }

        public bool DejaUtilisé(string code_)
        {
            /// Pour les badges des participants. il n'y a pas d'expiration.... donc on ne fait pas ce qui est écrit en dessous
            /// sauf celle-ci
            return false;

/*
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque;
            var __testEntree = (
                from c in __db.FLUXes
                where ((c.TypeFlux == 'E') && (c.Code == code_))
                select c
                ).Count();
            if (__testEntree <= 0) return false;
            var __testSortie = (
                from c in __db.FLUXes
                where ((c.TypeFlux == 'S') && (c.Code == code_))
                select c
                ).Count();
            if (__testSortie <= 0) return false;

            return true; // Il y a déja eu de "E/S" effectué par ce code barre.
*/
        }

        public List<InfoFluxClient> ListePassage(string nomPC_, int nbMax_) // OK angamba
        {
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque ;
            var __temporaire =
                (
                    from c in __db.FLUXes
                    where ((nomPC_ == "TOUS") || (c.PC_src == nomPC_))
                    orderby c.Id descending

                    join cd in __db.CODE_BARREs
                    on c.Code equals cd.Code
                    select new
                    {
                        flux = c,
                        code = cd
                    }
                ) ;
            if (nbMax_ > 0) __temporaire = __temporaire.Take(nbMax_);


            List<InfoFluxClient> __liste = 
                (
                    from temp in __temporaire
                    join p in __db.PERSONNEs
                    on temp.code.IdPers equals p.Id
                    select CréerInfoFluxClient(int.Parse(temp.code.Code),temp.flux.DateComplet.Value,temp.flux.TypeFlux.Value,temp.flux.DescriptionPortail, p)
                ).ToList() ;

            return __liste;
        }

        public int NombreTotalParticipant() // OK
        {
            try
            {
                return (
                    from pers in BaseDeDonnees.NouveauPetanque.PERSONNEs
                    where ((pers.Type == "Part") || (pers.Type == "Divers") || (pers.Type == "SP")||(pers.Type=="JB"))
                    select pers
                ).Count();
            }
            catch
            {
                return 0 ;
            }
        }
        public bool CodeBarreExiste(string code_) //OK
        {
            try
            {
                var __test = requeteCodeBarre(code_).Count();
                return (__test > 0);
            }
            catch
            {
                return false;
            }
        }
        public bool PeutPasserNiveau(string code_, int niveau_) // OK angamba
        {
            try
            {
                // Faute de conception, on doit verifier nous-même ici si le type de la personne est compatible avec le niveau requis
                CODE_BARRE __codeBarre = requeteCodeBarre(code_).FirstOrDefault();
                PERSONNE __p = requetePersonne(__codeBarre.IdPers).FirstOrDefault();

                return TypePersPeutPasserNiveau(__p.Autre_info, niveau_);
            }
            catch
            {
                return false;
            }

        }
        private bool TypePersPeutPasserNiveau(string type_, int niveau_)// OK angamba 
        {
            /// Au dernier moment, tout a changé, ne vous attendez pas à un miracle.... la solution adopté
            /// est du type "Ad-hoc"....
            ///Ce n'est pas le type mais "l'autre info"
            return true;

            /*
            if ((type_ == "VISITEUR") || (type_ == "PUBLIC") || (type_ == "JRL"))
            {
                return (niveau_ == 0); // C'est le seul que l'on puisse utiliser ici
            }
            if (type_ == "VIP")
            {
                return (niveau_ <= 1); // Arbitraire seulement
            }
            if (type_ == "PARTICIPANT")
            {
                return true; // Bon, ce n'est que temporaire....
            }
            return false;
            */
        }

        private IQueryable<CODE_BARRE> requeteCodeBarre(string code_)
        {
            return (
                    from code in BaseDeDonnees.NouveauPetanque.CODE_BARREs
                    where (code.Code == code_)
                    select code
                    );
        }
        private IQueryable<PERSONNE> requetePersonne(int? id_)
        {
            return (from p in BaseDeDonnees.NouveauPetanque.PERSONNEs
                    where (p.Id == id_)
                    select p);
        }


        public List<int> NombreFlux(DateTime d)
        {
            PetanqueDataContext _petanque = BaseDeDonnees.NouveauPetanque;
            List<int> _res = new List<int>();
            for (int i = 0; i <= 13; i++)
            {
                _res.Add(nbF(d, i, _petanque));
            }
            return _res;
        }
        private int nbF(DateTime d,int modeHeure, PetanqueDataContext db_)
        {
            DateTime t1 = DateTime.Now;
            DateTime t2 = DateTime.Now;
            if ((modeHeure >= 0) && (modeHeure <= 13))
            {
                t1 = new DateTime(d.Year, d.Month, d.Day, modeHeure + 6, 0, 1);
                t2 = new DateTime(d.Year, d.Month, d.Day, modeHeure + 7, 0, 0);
            }

            return (
                        from c in db_.FLUXes
                        where ((DateTime.Compare(c.DateComplet.Value, t1) >= 0) && (DateTime.Compare(c.DateComplet.Value, t2) <= 0))
                        select c
                    ).Count();
        }















        private int nombre2(string typePersonne_, ENombrePersonne e_, int niveau_, DateTime date_)
        {
            return (nombre("TOUS", e_, niveau_, date_) - nombre(typePersonne_,e_,niveau_,date_));
            // ty zany resaka "!=" ndray fa tsy "=="
        }

        public int NombrePersonne2(string typePersonne_, ENombrePersonne cumul_, int niveau_)
        {
            DateTime __date = DateTime.Now.Date;
            return nombre2(typePersonne_, cumul_, niveau_, __date);
        }

        public int NombreTotalAutre()
        {
            try
            {
                return (BaseDeDonnees.NouveauPetanque.PERSONNEs).Count() - NombreTotalParticipant();
            }
            catch
            {
                return 0;
            }
        }

        public List<InfoFluxClient> ListePassage2(string nomPC_, int nbMax_, DateTime tbegin_, DateTime tend_)
        {
            PetanqueDataContext __db = BaseDeDonnees.NouveauPetanque;
            var __temporaire =
                (
                    from c in __db.FLUXes
                    where (((nomPC_ == "TOUS") || (c.PC_src == nomPC_)) && ((DateTime.Compare(c.DateComplet.Value,tbegin_) >= 0) && (DateTime.Compare(c.DateComplet.Value, tend_) <= 0)))
                    orderby c.Id descending

                    join cd in __db.CODE_BARREs
                    on c.Code equals cd.Code
                    select new
                    {
                        flux = c,
                        code = cd
                    }
                );
            if (nbMax_ > 0) __temporaire = __temporaire.Take(nbMax_);


            List<InfoFluxClient> __liste =
                (
                    from temp in __temporaire
                    join p in __db.PERSONNEs
                    on temp.code.IdPers equals p.Id
                    select CréerInfoFluxClient(int.Parse(temp.code.Code), temp.flux.DateComplet.Value, temp.flux.TypeFlux.Value, temp.flux.DescriptionPortail, p)
                ).ToList();

            return __liste;
        }

    }
}
