﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVER
{
    // Singleton pattern
    static class BaseDeDonnees
    {
        private static PetanqueDataContext _petanque = null ;
        public static PetanqueDataContext Petanque { 
            get
            {
                if (_petanque == null)
                    _petanque = new PetanqueDataContext();
                return _petanque ;
            }
        }

        public static PetanqueDataContext NouveauPetanque
        {
            get
            {
                return new PetanqueDataContext();
            }
        }
        
        public static bool Test()
        {
            try
            {
                var __test = (from c in Petanque.CODE_BARREs select c).First() ;
                return true ;
            }
            catch
            {
                MessageBox.Show("Problème de connexion à la base de données...l'application va se fermer....", "ERREUR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false ;
            }
        }


        public static void InsererFLUX(FLUX p) // OK
        // On ne peut pas respecter le design Singleton pour l'insertion
        // Car il se pourrait qu'un Thread est en train de faire un "InsertOnSubmit()" pendant que d'autres font un "SubmitChanges()"
        // ce qui lève une exception
        {
            try
            {
                PetanqueDataContext __temporaire = new PetanqueDataContext();
                __temporaire.FLUXes.InsertOnSubmit(p);
                __temporaire.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
