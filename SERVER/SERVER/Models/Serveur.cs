﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using InterfaceComms;

namespace SERVER.Models
{
    public class Serveur
    {
        private string _ip;
        private int _port;
        private string _nomService;

        private CancellationTokenSource _wtoken;
        private Task _task;



        private TcpChannel _channel { get; set; }
        private ServerComm _objetDeCommunication { get; set; }


        public string IP
        {
            get
            {
                return _ip;
            }
            private set
            {
                try {
                    IPAddress[] __ipTest = Dns.GetHostAddresses(value);
                    int __l = __ipTest.Length - 1;
                    _ip = __ipTest[__l].ToString();
                } catch {
                    throw new Exception("Le format de l'addresse IP '" + value + "' est invalide");
                }
            }
        }
        public int Port
        {
            get
            {
                return _port;
            }
            private set
            {
                if ((value < 0) || (value > 65535)) // Max value
                    throw new Exception(
                        "Le port doit-être un nombre compris entre 1 et 65535..." + 
                        " La valeur actuelle " + value + " n'est pas acceptable."
                    );
                else
                    _port = value; 
            }
        }
        public Task Tache 
        {
            get 
            {
                return _task;
            }
            set 
            {
                try
                {
                    _task = value;
                }
                catch (Exception e_)
                {
                    throw e_;
                }
            }
        }
        public CancellationToken SToken
        {
            get
            {
                return _wtoken.Token ;
            }
        }



        public Serveur(int port_ = 8192, string ip_ = "0.0.0.0")
        {
            try
            {
                if (ip_ == "0.0.0.0")
                {
                    IP = _getLocalIP();
                }
                else
                {
                    IP = ip_ ;
                }
                Port = port_;
                _wtoken = new CancellationTokenSource();

            } catch (Exception e)
            {
                throw e;
            }
        }

        public void Lancer(string nomService_, ServerComm.MiseAJourHandler handler_ = null)
        {
            try {
                _channel = new TcpChannel(Port); // création d'un canal sur le port choisi
                ChannelServices.RegisterChannel(_channel, false);

                // activation coté serveur en mode Singleton
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(ServerComm), _nomService = nomService_, WellKnownObjectMode.Singleton); // On a besoin de ce modèle singleton pour pouvoir récupérer l'objet en question

                _objetDeCommunication = (ServerComm)((IServerComm)Activator.GetObject(typeof(IServerComm), "tcp://" + IP + ":" + Port + "/" + _nomService)).This ;

                if (handler_ != null)
                    _objetDeCommunication.MiseAJour_Event += handler_;
            } catch (Exception e)
            {
                throw e;
            }
        }
        public void Arreter()
        {
            try {
                IChannel[] __regChannels = ChannelServices.RegisteredChannels;
                if (__regChannels.Length > 0)
                {
                    //_wtoken.Cancel(false) ; // Il n'y a pas encore de tâche pour l'instant
                    ChannelServices.UnregisterChannel(_channel);
                }
            } catch (Exception e_){
                throw e_ ;
            }
        }

        private string _getLocalIP()
        {
            IPHostEntry _host;
            string _IPlocale = "0.0.0.0";
            _host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in _host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    _IPlocale = ip.ToString();
                }
            }
            return _IPlocale ;
        }
    }
}
