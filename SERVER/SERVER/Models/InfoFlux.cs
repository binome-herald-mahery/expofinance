﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVER.Models
{
    public class InfoFlux
    {
        [Browsable(false)]
        public int Id { get; set; }

        [Browsable(false)]
        public char? Type { get; set; }
        

        //PERSONNE ty fa ts tafa raha ts zao....
        [Browsable(false)]
        public string Nom { get; set; }
        [Browsable(false)]
        public string Prenom { get; set; }
        [Browsable(false)]
        public string Pays { get; set; }
        [Browsable(false)]
        public string Profession { get; set; }

        [Browsable(false)]
        public string CodeBarre { get; set; }


        public string DatePassage { get; set; }

        public string Description { get; set; }

        public string PC_Source { get; set; }

        public string DescriptionPortail { get; set; }

        public int? Niveau { get; set; }

        public InfoFlux(FLUX c, PERSONNE p)
        {
            Id = c.Id ;
            
            if (c.DateComplet.HasValue)
            {
                DatePassage = c.DateComplet.Value.ToString();
            }

            Type = c.TypeFlux;
            if (c.TypeFlux == 'E')
                Description = "Entrée : " ;
            else
                Description = "Sortie : " ;
            Nom = p.Nom;
            Prenom = "" ;
            Pays = p.Pays;
            Profession = p.Fonction ;
            CodeBarre = c.Code;

            Description += c.Code + ", " + p.Nom ;
            if ((p.Type != "") && (p.Type != null)) {
                Description += ", " + p.Type;
            }
            Description += ", " + Pays;

            PC_Source = c.PC_src ;
            
            Niveau = c.NiveauPortail;
            DescriptionPortail = c.DescriptionPortail;
        }
    }
}
