﻿using DevExpress.LookAndFeel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using SERVER.Forms;
    
namespace SERVER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        [STAThread]
        static void Main()
        {
            bool _clear = true ;
            // Configuration VS
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Configuration DevExpress
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
            UserLookAndFeel.Default.SkinName = "Liquid Sky";
            UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;

            // Si un splashscreen est lancé.
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }


/* DB */
                // WaitScreen....
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(AttenteBDconnexion));
            Application.DoEvents();

                // Test de la connexion à la base de données...
            _clear = _clear && BaseDeDonnees.Test() ;
            
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
/* /DB */


            if (_clear)
                Application.Run(new Principale());
        }
    }
}
