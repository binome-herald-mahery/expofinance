﻿using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraSplashScreen;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using SERVER.Models;

namespace SERVER.Forms
{
    public partial class Principale : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private int _srvPortNumber { get; set; }
        private string _nomService { get; set; }
        private int _nbInterieur { get; set; } // nombre de personnes actuellement à l'intérieur.

        private List<SERVER.Models.InfoFlux> _liste { get; set; }
        private int _nListMax = 10; // Nombre maximale à afficher dans l'évènement (_liste)
        private Serveur _serveur { get; set; }

        private bool _stopped = true ; // Indique l'état du serveur actuellement

        public Principale()//OK
        {
            // Lire les configurations....
            try
            {
                _srvPortNumber = Int32.Parse(ConfigurationManager.AppSettings["PORT"].ToString());
            }
            catch
            {
                _srvPortNumber = 0x4D4B; // Si il n'y a pas de valeur dans le "PORT" de la configuration
            }

            try
            {
                _nomService = ConfigurationManager.AppSettings["NOM_SERVICE"].ToString();
            }
            catch
            {
                _nomService = "SGV" ; // Par défaut
            }
            

            InitializeComponent();
            GridLocalizer.Active = new FrenchGridLocalizer(); // Rendre en Français les boutons de recherches dans le tableau.

            _liste = new List<SERVER.Models.InfoFlux>();

            majListe(); // Mise a jour de la liste....
            lier(); // Lier la liste à la table...

            _modeMarche(false); // Serveur "OFF"

            //Création du Serveur en mode local (IP local)
            _serveur = new Serveur(_srvPortNumber,Environment.MachineName);

            labelIP.Text = "IP Server  :  " + _serveur.IP; // On affiche l'adresse IP du serveur
        }
        private void _modeMarche(bool enMarche) // OK
        {
            btnStart.Enabled = !enMarche;
            btnStop.Enabled = enMarche;
            _stopped = !enMarche;
        }
        private void btnStart_Click(object sender, EventArgs e)//OK
        {
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            if (!_stopped) return; // Juste au cas où...

            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(AttenteLancement));
            Application.DoEvents();


            try // test de création d'un canal et d'activation des objets
            {
                _serveur.Lancer(_nomService,miseAJourDonnees);

                labelMsg.Text = "Le serveur a démarré sur le port " + _srvPortNumber.ToString();
                labelMsg.ForeColor = Color.SteelBlue;

                _modeMarche(true);
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                XtraMessageBox.Show(defaultLookAndFeel1.LookAndFeel, "Erreur de demarrage de service", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
        }
        private void btnStop_Click(object sender, EventArgs e)//OK
        {
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(AttenteArret));
            Application.DoEvents();

            try
            {
                _serveur.Arreter();
                labelMsg.Text = "Le serveur a été arreté";
                labelMsg.ForeColor = Color.Firebrick;

                _modeMarche(false);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                XtraMessageBox.Show(defaultLookAndFeel1.LookAndFeel, "Erreur d'arret de service", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
        }
        private void Principale_FormClosing(object sender, FormClosingEventArgs e)//OK
        {
            if (_stopped)
            {
                try
                {
                    _serveur.Arreter();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    XtraMessageBox.Show(defaultLookAndFeel1.LookAndFeel, "Erreur d'arret de service", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                // Pour que le serveur ne s'arrête pas par accident ci facilement...
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized; // pour animation....
                Thread.Sleep(100);
                this.Visible = false;
                notifier("Le serveur continue de fonctionner en arrière-plan", "Information", ToolTipIcon.Info);
            }
        }
        private void notifier(string texte, string titre = "", ToolTipIcon t = ToolTipIcon.None, int dureems = 1000)//OK
        {
            notification.Visible = true;
            notification.BalloonTipText = texte ;
            notification.BalloonTipTitle = titre;
            notification.BalloonTipIcon = t;
            notification.ShowBalloonTip(dureems);
            notification.Text = "Serveur\n" + 
                                "Adresse IP: " + _serveur.IP + 
                                "\nPort: " + _serveur.Port.ToString() ;
        }
        private void notification_Click(object sender, EventArgs e) //OK
        {
            this.Visible = true;
            notification.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }
        private void lier() // OK
        {
            BindingSource bind = new BindingSource();
            bind.DataSource = _liste;
            this.gridControl1.DataSource = bind;
        }
        private void majListe() //OK
        {
            if (_liste == null) _liste = new List<Models.InfoFlux>();
            _liste.Clear();

            if (_nbInterieur <= 0)
                _liste = prendreListe().ToList() ; // Prend les derniers "_nListMax" tout au plus
            else
                _liste = prendreListe().Take(_nListMax).ToList(); // Prend les derniers "_nListMax"

            _nbInterieur = (
                    ( // Nombre d'entrée dans la porte principale (niveau 0)
                        from c in BaseDeDonnees.Petanque.FLUXes
                        where ((c.TypeFlux == 'E') && (c.NiveauPortail == 0))
                        select c
                    ).Count() 
                    -
                    ( // Nombre de sorties dans la porte principale (niveau 0)
                        from c in BaseDeDonnees.Petanque.FLUXes
                        where ((c.TypeFlux == 'S') && (c.NiveauPortail == 0))
                        select c
                    ).Count()
                ) ;
            lNbPers.Text = _nbInterieur.ToString();
        }
        private IQueryable<InfoFlux> prendreListe() // OK
        {
            return (
                    from c in BaseDeDonnees.Petanque.FLUXes
                    join code in BaseDeDonnees.Petanque.CODE_BARREs
                    on c.Code equals code.Code
                    orderby c.Id descending

                    join p in BaseDeDonnees.Petanque.PERSONNEs
                    on code.IdPers equals p.Id

                    select new Models.InfoFlux(c, p)
                );
        }
        private void ajouterInfo(InfoFlux e_) // OK
        {
            int __n = _liste.Count;

            _liste.Insert(0, e_); // ATTENTION, après ceci, on a (__n == _liste.Count - 1)..... attention

            if ((_nListMax > 0) && (__n >= _nListMax))
            {
                _liste.RemoveAt(__n);
            }

            if ((e_.Niveau == 0) && (e_.Type.HasValue))
                if (e_.Type == 'E')
                    _nbInterieur++;
                else if (e_.Type == 'S') // Juste au cas où il y aurait un changement
                    _nbInterieur--;
            lNbPers.Text = _nbInterieur.ToString();
            lier(); // "Rafraichir la liste"
        }
        private void miseAJourDonnees(SERVER.Models.InfoFlux e_) //OK, "Evènement" appelée par le thread de ServerComm
        {
            // Pour l'instant seulement... on peut y ajouter des "actions"
            this.BeginInvoke(new MethodInvoker(
                    () =>
                    {
                        ajouterInfo(e_);
                    }
                )
            );
        }

        private void lNbPers_Click(object sender, EventArgs e)
        {

        }

    }
}
