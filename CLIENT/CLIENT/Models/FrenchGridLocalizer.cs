﻿using DevExpress.XtraGrid.Localization ;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLIENT.Models
{
    public class FrenchGridLocalizer : GridLocalizer
    {
        public override string GetLocalizedString(GridStringId id)
        {
            switch (id)
            {
                case GridStringId.FindControlFindButton:
                    return "Rechercher";
                case GridStringId.FindControlClearButton:
                    return "Effacer";
                case GridStringId.FilterPanelCustomizeButton:
                    return "Editer";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }
}
