﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;


using InterfaceComms;
using System.Windows.Forms;

namespace CLIENT.Models
{
    public class ConnexionServeur
    {
        public delegate void TacheHandler(CancellationToken c) ;


        private string _adresseCanal;
        public string AdresseCanal
        {
            get
            {
                return _adresseCanal;
            }
            set
            {
                _adresseCanal = value;
            }
        }

        private TcpChannel _canal;
        public TcpChannel Canal
        {
            get
            {
                return _canal;
            }
            set
            {
                _canal = value;
            }
        }

        private CancellationTokenSource _wtoken;
        public CancellationTokenSource WToken
        {
            get 
            {
                return _wtoken;
            }
            set
            {
                _wtoken = value;
            }
        }
        public CancellationToken SToken
        {
            get 
            {
                return _wtoken.Token;
            }
        }

        private Task _tache;
        public Task Tache
        {
            get
            {
                return _tache;
            }
            set
            {
                _tache = value;
            }
        }

        private bool _connecté;
        public bool Connected
        {
            get 
            {
                return _connecté;
            }
            private set
            {
                _connecté = value;
            }
        }

        private IServerComm _connexion;
        public IServerComm Interface
        {
            get
            {
                return _connexion;
            }
            set
            {
                _connexion = value;
            }
        }

        public ConnexionServeur(string ip_ = "127.0.0.1", int port_ = 8192, string nomService_ = "SGV") //OK
        {
            try
            {
                AdresseCanal = "tcp://" + ip_ + ":" + port_ + "/" + nomService_; // création de l'url de connexion
                TcpChannel channel = new TcpChannel();
                ChannelServices.RegisterChannel(channel, false);
                Connected = false ;
            }
            catch (Exception e_)
            {
                throw e_;
            }
        }

        public void Connecter() //OK
        {
            try
            {
                Interface = (IServerComm)Activator.GetObject(typeof(IServerComm), AdresseCanal);
                WToken = new CancellationTokenSource();
                Connected = true;
            }
            catch (Exception e_)
            {
                throw e_;
            }
        }

        public void LancerTache(TacheHandler methode_ = null) //OK
        {
            try
            {
                Tache = Task.Factory.StartNew(() => methode_(SToken),SToken);
            }
            catch (Exception e_)
            {
                Connected = false;
                throw e_;
            }
        }

        public void Fermer() //OK
        {
            WToken.Cancel(true) ;
            Connected = false;
        }
    }
}
