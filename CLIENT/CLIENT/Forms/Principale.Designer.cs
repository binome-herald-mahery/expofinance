﻿// A effacer si ca va beee...
using System.Threading;

namespace CLIENT.Forms
{
    partial class Principale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager manager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::CLIENT.Forms.SplashPCP), true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principale));
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.StatusInfo = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.GroupeVisiteur = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupePassageTemporaire = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.tableauVisiteur = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ctrlTableau = new DevExpress.XtraGrid.GridControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this.visitorGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.addVisitorItem = new DevExpress.XtraNavBar.NavBarItem();
            this.delVisitorItem = new DevExpress.XtraNavBar.NavBarItem();
            this.organizerGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.statistiqueItem = new DevExpress.XtraNavBar.NavBarItem();
            this.exportRapportItem = new DevExpress.XtraNavBar.NavBarItem();
            this.controleTableau = new DevExpress.XtraGrid.GridControl();
            this.tableFLUX = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblValJeuBadge = new DevExpress.XtraEditors.LabelControl();
            this.lCumulVisit = new DevExpress.XtraEditors.LabelControl();
            this.lblValDivers = new DevExpress.XtraEditors.LabelControl();
            this.lPAuj = new DevExpress.XtraEditors.LabelControl();
            this.lblValStaffPublic = new DevExpress.XtraEditors.LabelControl();
            this.lblStaffPublic = new DevExpress.XtraEditors.LabelControl();
            this.lblValParticipantInt = new DevExpress.XtraEditors.LabelControl();
            this.lParticipantInt = new DevExpress.XtraEditors.LabelControl();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauVisiteur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTableau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controleTableau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableFLUX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.barStaticItem1,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barCheckItem1,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.StatusInfo});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 13;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.TabletOfficeEx;
            this.ribbon.Size = new System.Drawing.Size(1173, 81);
            this.ribbon.StatusBar = this.ribbonStatusBar1;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Above;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Prêt";
            this.barStaticItem1.Id = 12;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Passage";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exporter";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 3;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Entrée";
            this.barButtonItem4.Id = 5;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Sortie";
            this.barButtonItem5.Id = 6;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Base de données";
            this.barButtonItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.Glyph")));
            this.barButtonItem6.Id = 7;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Accès utilisateur";
            this.barButtonItem7.Id = 8;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // StatusInfo
            // 
            this.StatusInfo.Caption = "Prêt";
            this.StatusInfo.Id = 12;
            this.StatusInfo.Name = "StatusInfo";
            this.StatusInfo.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.GroupeVisiteur,
            this.GroupePassageTemporaire});
            this.ribbonPage1.ImageAlign = DevExpress.Utils.HorzAlignment.Center;
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "FICHIER";
            // 
            // GroupeVisiteur
            // 
            this.GroupeVisiteur.Glyph = ((System.Drawing.Image)(resources.GetObject("GroupeVisiteur.Glyph")));
            this.GroupeVisiteur.ItemLinks.Add(this.barButtonItem1);
            this.GroupeVisiteur.ItemLinks.Add(this.barButtonItem2);
            this.GroupeVisiteur.Name = "GroupeVisiteur";
            this.GroupeVisiteur.Text = "Visiteur";
            // 
            // GroupePassageTemporaire
            // 
            this.GroupePassageTemporaire.ItemLinks.Add(this.barButtonItem4);
            this.GroupePassageTemporaire.ItemLinks.Add(this.barButtonItem5);
            this.GroupePassageTemporaire.Name = "GroupePassageTemporaire";
            this.GroupePassageTemporaire.Text = "Passage temporaire";
            this.GroupePassageTemporaire.Visible = false;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "PARAMETRES";
            this.ribbonPage2.Visible = false;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Système";
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.LookAndFeel.SkinName = "Liquid Sky";
            this.repositoryItemTimeEdit1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.StatusInfo);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 629);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbon;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1173, 25);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = null;
            this.gridSplitContainer1.Location = new System.Drawing.Point(227, 210);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Size = new System.Drawing.Size(683, 303);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // tableauVisiteur
            // 
            this.tableauVisiteur.GridControl = this.ctrlTableau;
            this.tableauVisiteur.Name = "tableauVisiteur";
            this.tableauVisiteur.OptionsBehavior.Editable = false;
            this.tableauVisiteur.OptionsFind.AlwaysVisible = true;
            this.tableauVisiteur.OptionsFind.FindNullPrompt = "Rechercher...";
            this.tableauVisiteur.OptionsView.ShowGroupPanel = false;
            // 
            // ctrlTableau
            // 
            this.ctrlTableau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrlTableau.Location = new System.Drawing.Point(227, 314);
            this.ctrlTableau.MainView = this.tableauVisiteur;
            this.ctrlTableau.MenuManager = this.ribbon;
            this.ctrlTableau.Name = "ctrlTableau";
            this.ctrlTableau.Size = new System.Drawing.Size(776, 303);
            this.ctrlTableau.TabIndex = 6;
            this.ctrlTableau.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tableauVisiteur});
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 81);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.navBarControl);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.controleTableau);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1173, 548);
            this.splitContainerControl1.SplitterPosition = 146;
            this.splitContainerControl1.TabIndex = 11;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // navBarControl
            // 
            this.navBarControl.ActiveGroup = this.visitorGroup;
            this.navBarControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.visitorGroup,
            this.organizerGroup});
            this.navBarControl.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.addVisitorItem,
            this.delVisitorItem,
            this.statistiqueItem,
            this.exportRapportItem});
            this.navBarControl.Location = new System.Drawing.Point(0, 0);
            this.navBarControl.Name = "navBarControl";
            this.navBarControl.OptionsNavPane.ExpandedWidth = 146;
            this.navBarControl.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControl.Size = new System.Drawing.Size(146, 548);
            this.navBarControl.StoreDefaultPaintStyleName = true;
            this.navBarControl.TabIndex = 2;
            this.navBarControl.Text = "navBarControl1";
            // 
            // visitorGroup
            // 
            this.visitorGroup.Caption = "Visiteur";
            this.visitorGroup.Expanded = true;
            this.visitorGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.addVisitorItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.delVisitorItem)});
            this.visitorGroup.LargeImage = ((System.Drawing.Image)(resources.GetObject("visitorGroup.LargeImage")));
            this.visitorGroup.LargeImageIndex = 0;
            this.visitorGroup.Name = "visitorGroup";
            // 
            // addVisitorItem
            // 
            this.addVisitorItem.Caption = "Enregistrer passage";
            this.addVisitorItem.LargeImage = ((System.Drawing.Image)(resources.GetObject("addVisitorItem.LargeImage")));
            this.addVisitorItem.Name = "addVisitorItem";
            this.addVisitorItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("addVisitorItem.SmallImage")));
            this.addVisitorItem.SmallImageIndex = 0;
            this.addVisitorItem.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.addVisitorItem_LinkClicked);
            // 
            // delVisitorItem
            // 
            this.delVisitorItem.Caption = "Supprimer passage";
            this.delVisitorItem.LargeImage = ((System.Drawing.Image)(resources.GetObject("delVisitorItem.LargeImage")));
            this.delVisitorItem.Name = "delVisitorItem";
            this.delVisitorItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("delVisitorItem.SmallImage")));
            this.delVisitorItem.SmallImageIndex = 1;
            this.delVisitorItem.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.delVisitorItem_LinkClicked);
            // 
            // organizerGroup
            // 
            this.organizerGroup.Caption = "Rapport";
            this.organizerGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.statistiqueItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.exportRapportItem)});
            this.organizerGroup.LargeImage = ((System.Drawing.Image)(resources.GetObject("organizerGroup.LargeImage")));
            this.organizerGroup.LargeImageIndex = 1;
            this.organizerGroup.Name = "organizerGroup";
            // 
            // statistiqueItem
            // 
            this.statistiqueItem.Caption = "Statistique";
            this.statistiqueItem.LargeImage = ((System.Drawing.Image)(resources.GetObject("statistiqueItem.LargeImage")));
            this.statistiqueItem.Name = "statistiqueItem";
            this.statistiqueItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("statistiqueItem.SmallImage")));
            this.statistiqueItem.SmallImageIndex = 4;
            this.statistiqueItem.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.statistiqueItem_LinkClicked);
            // 
            // exportRapportItem
            // 
            this.exportRapportItem.Caption = "Affluence";
            this.exportRapportItem.LargeImage = ((System.Drawing.Image)(resources.GetObject("exportRapportItem.LargeImage")));
            this.exportRapportItem.Name = "exportRapportItem";
            this.exportRapportItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("exportRapportItem.SmallImage")));
            this.exportRapportItem.SmallImageIndex = 5;
            this.exportRapportItem.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.exportRapportItem_LinkClicked);
            // 
            // controleTableau
            // 
            this.controleTableau.Cursor = System.Windows.Forms.Cursors.Default;
            this.controleTableau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controleTableau.Location = new System.Drawing.Point(0, 160);
            this.controleTableau.MainView = this.tableFLUX;
            this.controleTableau.MenuManager = this.ribbon;
            this.controleTableau.Name = "controleTableau";
            this.controleTableau.Size = new System.Drawing.Size(1023, 388);
            this.controleTableau.TabIndex = 0;
            this.controleTableau.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tableFLUX});
            this.controleTableau.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridControl1_KeyUp);
            // 
            // tableFLUX
            // 
            this.tableFLUX.Appearance.EvenRow.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.tableFLUX.Appearance.EvenRow.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.tableFLUX.Appearance.EvenRow.Options.UseBackColor = true;
            this.tableFLUX.GridControl = this.controleTableau;
            this.tableFLUX.Name = "tableFLUX";
            this.tableFLUX.OptionsBehavior.Editable = false;
            this.tableFLUX.OptionsFind.AlwaysVisible = true;
            this.tableFLUX.OptionsFind.FindNullPrompt = "Rechercher...";
            this.tableFLUX.OptionsView.EnableAppearanceEvenRow = true;
            this.tableFLUX.OptionsView.EnableAppearanceOddRow = true;
            this.tableFLUX.OptionsView.ShowGroupPanel = false;
            this.tableFLUX.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSize = true;
            this.groupControl1.Controls.Add(this.lblValJeuBadge);
            this.groupControl1.Controls.Add(this.lCumulVisit);
            this.groupControl1.Controls.Add(this.lblValDivers);
            this.groupControl1.Controls.Add(this.lPAuj);
            this.groupControl1.Controls.Add(this.lblValStaffPublic);
            this.groupControl1.Controls.Add(this.lblStaffPublic);
            this.groupControl1.Controls.Add(this.lblValParticipantInt);
            this.groupControl1.Controls.Add(this.lParticipantInt);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1023, 160);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Affluence";
            // 
            // lblValJeuBadge
            // 
            this.lblValJeuBadge.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValJeuBadge.Location = new System.Drawing.Point(858, 92);
            this.lblValJeuBadge.Name = "lblValJeuBadge";
            this.lblValJeuBadge.Size = new System.Drawing.Size(144, 45);
            this.lblValJeuBadge.TabIndex = 13;
            this.lblValJeuBadge.Text = "999999";
            // 
            // lCumulVisit
            // 
            this.lCumulVisit.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCumulVisit.Location = new System.Drawing.Point(535, 92);
            this.lCumulVisit.Name = "lCumulVisit";
            this.lCumulVisit.Size = new System.Drawing.Size(258, 45);
            this.lCumulVisit.TabIndex = 12;
            this.lCumulVisit.Text = "Jeu de badges :";
            // 
            // lblValDivers
            // 
            this.lblValDivers.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValDivers.Location = new System.Drawing.Point(859, 31);
            this.lblValDivers.Name = "lblValDivers";
            this.lblValDivers.Size = new System.Drawing.Size(144, 45);
            this.lblValDivers.TabIndex = 11;
            this.lblValDivers.Text = "999999";
            // 
            // lPAuj
            // 
            this.lPAuj.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPAuj.Location = new System.Drawing.Point(535, 31);
            this.lPAuj.Name = "lPAuj";
            this.lPAuj.Size = new System.Drawing.Size(125, 45);
            this.lPAuj.TabIndex = 10;
            this.lPAuj.Text = "Divers :";
            // 
            // lblValStaffPublic
            // 
            this.lblValStaffPublic.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValStaffPublic.Location = new System.Drawing.Point(335, 92);
            this.lblValStaffPublic.Name = "lblValStaffPublic";
            this.lblValStaffPublic.Size = new System.Drawing.Size(144, 45);
            this.lblValStaffPublic.TabIndex = 9;
            this.lblValStaffPublic.Text = "999999";
            // 
            // lblStaffPublic
            // 
            this.lblStaffPublic.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStaffPublic.Location = new System.Drawing.Point(18, 92);
            this.lblStaffPublic.Name = "lblStaffPublic";
            this.lblStaffPublic.Size = new System.Drawing.Size(280, 45);
            this.lblStaffPublic.TabIndex = 8;
            this.lblStaffPublic.Text = "Staff PUBLICIX : ";
            // 
            // lblValParticipantInt
            // 
            this.lblValParticipantInt.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValParticipantInt.Location = new System.Drawing.Point(335, 31);
            this.lblValParticipantInt.Name = "lblValParticipantInt";
            this.lblValParticipantInt.Size = new System.Drawing.Size(144, 45);
            this.lblValParticipantInt.TabIndex = 5;
            this.lblValParticipantInt.Text = "999999";
            // 
            // lParticipantInt
            // 
            this.lParticipantInt.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lParticipantInt.Location = new System.Drawing.Point(18, 31);
            this.lParticipantInt.Name = "lParticipantInt";
            this.lParticipantInt.Size = new System.Drawing.Size(271, 45);
            this.lParticipantInt.TabIndex = 4;
            this.lParticipantInt.Text = "Participants      :";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.GridControl = this.controleTableau;
            // 
            // Principale
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1173, 654);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ctrlTableau);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Principale";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "PORTAIL - Code Barre - Expofinance Mada2017";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Principale_FormClosing);
            this.Load += new System.EventHandler(this.Principale_Load);
            this.Shown += new System.EventHandler(this.Principale_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableauVisiteur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTableau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controleTableau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableFLUX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Views.Grid.GridView tableauVisiteur;
        private DevExpress.XtraGrid.GridControl ctrlTableau;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraNavBar.NavBarControl navBarControl;
        private DevExpress.XtraNavBar.NavBarGroup visitorGroup;
        private DevExpress.XtraNavBar.NavBarItem addVisitorItem;
        private DevExpress.XtraNavBar.NavBarItem delVisitorItem;
        private DevExpress.XtraNavBar.NavBarGroup organizerGroup;
        private DevExpress.XtraNavBar.NavBarItem statistiqueItem;
        private DevExpress.XtraNavBar.NavBarItem exportRapportItem;
        private DevExpress.XtraGrid.GridControl controleTableau;
        private DevExpress.XtraGrid.Views.Grid.GridView tableFLUX;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupeVisiteur;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupePassageTemporaire;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraBars.BarStaticItem StatusInfo;
        //private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1 ;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lblValJeuBadge;
        private DevExpress.XtraEditors.LabelControl lCumulVisit;
        private DevExpress.XtraEditors.LabelControl lblValDivers;
        private DevExpress.XtraEditors.LabelControl lPAuj;
        private DevExpress.XtraEditors.LabelControl lblValStaffPublic;
        private DevExpress.XtraEditors.LabelControl lblStaffPublic;
        private DevExpress.XtraEditors.LabelControl lblValParticipantInt;
        private DevExpress.XtraEditors.LabelControl lParticipantInt;
    }
}