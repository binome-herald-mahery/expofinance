﻿using DevExpress.LookAndFeel;
using DevExpress.XtraBars ;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Localization ;
using DevExpress.XtraSplashScreen;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Reflection;

using CLIENT.Models;

using InterfaceComms;

namespace CLIENT.Forms
{
    public partial class Principale : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private AjoutVisiteurPortail _ajoutVisiteurPortail { get; set; }
        private char _type_par_defaut { get; set; }
        private char _mode_appli { get; set; }
        private string _type_portail { get; set; }
        private int _niveauPortail { get; set; }
        private string _nomService { get; set; }

        private string _cheminOK = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),@".\SONS\OK.mp3");
        private string _cheminERROR = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @".\SONS\ERROR.mp3");


        private int _nbMaxAffichage = -1 ; // On peut le changer....
        private bool _toutPC = false;  // Afficher tous les ordi....
        // Mode admin ou pas .... changer quand on doit le faire...
        private bool _toutAffichage = false; // Afficher toute la liste des passages....


        private List<InfoFluxClient> _listeDonneesTable { get; set; }
        private ConnexionServeur _objetConnexion { get; set; }

        private void MAJCompteur()
        {
            int __valParticipant = 0;
            int __valSP = 0;
            int __valDivers = 0;
            int __valJB = 0;

            try
            { //// Afficher chaque nombre qui nous intéresse.....
                // Ad-hoc comme d'hab...
                __valParticipant = _objetConnexion.Interface.NombrePersonne("Part", ENombrePersonne.ACTUEL, _niveauPortail);
                __valSP = _objetConnexion.Interface.NombrePersonne("Divers", ENombrePersonne.ACTUEL, _niveauPortail);
                __valDivers = _objetConnexion.Interface.NombrePersonne("SP", ENombrePersonne.ACTUEL, _niveauPortail);
                __valJB = _objetConnexion.Interface.NombrePersonne("JB", ENombrePersonne.ACTUEL, _niveauPortail);

                //__valVSTNow = _objetConnexion.Interface.NombrePersonne("TOUS", ENombrePersonne.ACTUEL, _niveauPortail) - __valPCPInt;
                //__valVSTCumul = _objetConnexion.Interface.NombreTotalAutre(); // OK!

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                this.Invoke(new MethodInvoker(delegate
                {
                    XtraMessageBox.Show("Erreur de connexion au serveur. L'Application va se fermer.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }));
            }

            lblValParticipantInt.BeginInvoke(new MethodInvoker(delegate
            {
                lblValParticipantInt.Text = __valParticipant.ToString();
            }));

            lblValStaffPublic.BeginInvoke(new MethodInvoker(delegate
            {
                lblValStaffPublic.Text = __valSP.ToString(); //valPCPExt.ToString();
            }));

            lblValDivers.BeginInvoke(new MethodInvoker(delegate
            {
                lblValDivers.Text = __valDivers.ToString();
            }));

            lblValJeuBadge.BeginInvoke(new MethodInvoker(delegate
            {
                lblValJeuBadge.Text = __valJB.ToString();
            }));
        }
        private InfoFluxClient EnregistrerVisiteur(string visiteurCode_, string fluxControle_, DateTime datePassage_) // Tokony OK
        {
            InfoFluxClient __retval = null;

            if (visiteurCode_ == null)
            {
                XtraMessageBox.Show(this, "Erreur de code. Veuillez réessayer!", "Gestion de Visiteurs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            try
            {
                int __code = Convert.ToInt32(visiteurCode_);
                string __messageErreur = "";
                bool __ok = false;

                // On procède comme ceci pour que l'on arrête la communication dès qu'il y a une erreur....
                if (_objetConnexion.Interface.CodeBarreExiste(visiteurCode_)) // Teste si le code barre existe dans la liste....
                {
                    if ((_objetConnexion.Interface.PasseFiltre(visiteurCode_,_type_portail)) && (_objetConnexion.Interface.PeutPasserNiveau(visiteurCode_, _niveauPortail))) // Teste si la personne peut passer cette portail (Teste si la personne peut passer ce niveau (E/S))
                    {
                        if (_objetConnexion.Interface.PasEncoreValidé(visiteurCode_, fluxControle_, _niveauPortail)) // Teste si la personne peut encore faire cette action (E ou S) dans ce niveau (ou que c'est déjà fait)
                        {
                            if (!_objetConnexion.Interface.DejaUtilisé(visiteurCode_)) // Teste si le code barre est déjà utilisé durant l'évènement (i.e en 'E' et 'S' au niveau 0!!!!)
                            {
                                if (_objetConnexion.Interface.PeutFaireAction(fluxControle_, _niveauPortail))
                                // Si flux controle = 'S' alors c'est ok, sauf si le nb de personnes est déjà nul (qui est impossible, mais il faut tout prévoir)
                                // Si flux controle = 'E' alors il faut que le nombre de personnes à l'intérieur n'est pas encore le maximum possible pour pouvoir y entrer
                                {
                                    __retval = _objetConnexion.Interface.AjouterVisiteurPassage(__code, datePassage_, fluxControle_[0], Environment.MachineName, _type_portail , _niveauPortail);
                                    __ok = true;
                                }
                                else // Ne peut pas (plus) faire l'action
                                {
                                    if (fluxControle_ == "E")
                                    {
                                        __messageErreur = "Nombre de personnes maximum atteint !!!";
                                    }
                                    else if (fluxControle_ == "S")
                                    {
                                        __messageErreur = "Plus de personne à l'intérieur !!!";
                                    }
                                }
                            }
                            else // Le code barre ne doit plus être utilisé....
                            {
                                __messageErreur = "Ce numéro de code barre est expiré !!!";
                            }
                        }
                        else // Déjà validé ou n'est pas encore passé à l'autre type de portail.
                        {
                            if (fluxControle_ == "E")
                            {
                                __messageErreur = "Entrée refusée! Présentez-vous d'abord à la sortie!"; // Si l'entrée est déjà validée
                            }
                            else if (fluxControle_ == "S")
                            {
                                __messageErreur = "Sortie refusée! Présentez-vous d'abord à l'entrée!"; // Si il n'a pas validé en entré ou si il est déjà sorti.
                            }
                        }
                    }
                    else // Niveau inaccessible à la code barre.
                    {
                        __messageErreur = "L'accès à cet endroit vous est refusé!";
                    }
                }
                else // Code barre non valide.
                {
                    __messageErreur = "Ce numéro de code barre est INVALIDE!";
                }

                if (__ok)
                {
                    jouerSon(_cheminOK);
                }
                else
                {
                    jouerSon(_cheminERROR);
                    Message __erreur = new Message(__messageErreur);
                    __erreur.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Invoke(new MethodInvoker(delegate
                {
                    XtraMessageBox.Show(this, "Erreur d'enregistrement. Veuillez réessayer!", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }));
            }
            return __retval;
        }
        private void jouerSon(string chemin_)
        {
            WMPLib.WindowsMediaPlayer Player = new WMPLib.WindowsMediaPlayer();
            Player.URL = chemin_ ;
            Player.controls.play();
        }
        private void insertionDonnees()
        {
            try //tentavive d'ajout de la liste de passage....
            {
                if (_objetConnexion.Connected)
                {
                    string __nomPC = Environment.MachineName;
                    int __nbMax = _nbMaxAffichage;
                    if (_toutPC) __nomPC = "TOUS";
                    if (_toutAffichage) __nbMax = -1;

                    _listeDonneesTable = _objetConnexion.Interface.ListePassage(__nomPC, __nbMax);
                    lier();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                XtraMessageBox.Show("Erreur de connexion au serveur. L'Application va se fermer.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
        }


        private void Principale_Shown(object sender, EventArgs e) //OK
        {
            _objetConnexion.LancerTache(tacheArrierePlan);
        }
        private void tacheArrierePlan(CancellationToken c) //OK
        {
            while (true)
            {
                if (c.IsCancellationRequested) break;

                MAJCompteur(); // Mise a jour des nombres de visiteurs en Entrée/Sortie

                Thread.Sleep(3000); // Iteration chaque 3 secondes
            }
        }
        public Principale() // OK
        {
            InitializeComponent();
            GridLocalizer.Active = new FrenchGridLocalizer();
            initialiserTableFLUX();

            string __serverIP = "";
            string __serverPort = "";
            try // tentative de lecture du fichier app.config
            {
                _type_par_defaut = ConfigurationManager.AppSettings["TYPE_DEFAUT"].ToString()[0];
                _mode_appli = ConfigurationManager.AppSettings["MODE"].ToString()[0];
                _niveauPortail = int.Parse(ConfigurationManager.AppSettings["NIVEAU_PORTAIL"].ToString());
                _nomService = ConfigurationManager.AppSettings["NOM_SERVICE"].ToString();
                _type_portail = ConfigurationManager.AppSettings["TYPE_PORTAIL"].ToString();


                __serverIP = ConfigurationManager.AppSettings["IP_SERV"];
                __serverPort = ConfigurationManager.AppSettings["PORT"];
            }
            catch (RemotingException ex)
            {
                XtraMessageBox.Show("Erreur de lecture du fichier app.config (" + ex.Message + ")");
                Environment.Exit(0);
            }

            try // tentative de connexion avec l'ip fourni par l'utilisateur
            {
                _objetConnexion = new ConnexionServeur(__serverIP, int.Parse(__serverPort), _nomService);
                _objetConnexion.Connecter();
            }
            catch (RemotingException ex)
            {
                XtraMessageBox.Show("Erreur lors de la tentative de connexion au serveur (" + ex.Message + ")");
                Environment.Exit(0);
            }

            _listeDonneesTable = new List<InfoFluxClient>();
            lier();
        }
        private void Principale_Load(object sender, EventArgs e) // OK
        {
            insertionDonnees();
        }
        private void insertionFlux(InfoFluxClient infoFlux) // OK... angamba
        {
            int __n = _listeDonneesTable.Count;
            _listeDonneesTable.Insert(0, infoFlux); // Après ceci, __n = _listeDonneesTable.Count-1 ... Attention
            if ((_nbMaxAffichage > 0) && (__n >= _nbMaxAffichage)) // Il suffit que nbMaxAffichage <= 0 pour que l'on n'ait aucune limite...
            {
                _listeDonneesTable.RemoveAt(__n);
            }
            lier();
        }
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e) // OK
        {
            if (_ajoutVisiteurPortail == null)
            {
                _ajoutVisiteurPortail = new AjoutVisiteurPortail(_type_par_defaut);
                _ajoutVisiteurPortail.StartPosition = FormStartPosition.CenterScreen;

                _ajoutVisiteurPortail.Action += this.ActionAjoutVisiteur;
            }
            _ajoutVisiteurPortail.TopLevel = true;
            this.Enabled = false;
            _ajoutVisiteurPortail.Show();
        }
        private void ActionAjoutVisiteur(VisiteurPassage Resultat, bool insertion)// OK jusqu'à maintenant (attente des bugs possibles)
        {
            if (!insertion)
            {
                _ajoutVisiteurPortail.TopLevel = false;
                _ajoutVisiteurPortail.Dispose();
                _ajoutVisiteurPortail = null;
                this.Enabled = true;
                return;
            }

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(AttenteEnregistrement));

            Application.DoEvents();

            InfoFluxClient __resultat = EnregistrerVisiteur(Resultat.NumBadge.ToString(), Resultat.TypeFlux, Resultat.DatePassage);


            if (__resultat != null)
            {
                this.Invoke(
                    new MethodInvoker(
                        delegate
                        {
                        insertionFlux(
                            __resultat
                            );
                        }
                    )
                );

                _ajoutVisiteurPortail.EffacerZoneTexte();
                _ajoutVisiteurPortail.ConsoleLog(
                    __resultat.TypeDeFlux + " de: " + "'" + __resultat.CodeBarre + "', Heure: " + __resultat.date.TimeOfDay.ToString()
                );
            }
            else
            {
                // ??? invalide ou quelqes choses comme cela... efa voatraite ao am EnregistrerVisiteur aloh n sasany eh
            }

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            _ajoutVisiteurPortail.ReFocus();
        }
        private void addVisitorItem_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e) // OK
        {
            barButtonItem1_ItemClick(sender, null);
        }
        private void delVisitorItem_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e) //OK, mais non pour futur exploitation
        {
            XtraMessageBox.Show(this, "Vous ne pouvez pas supprimer la trace de passage!", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void gridControl1_KeyUp(object sender, KeyEventArgs e) // OK, mais non pour futur exploitation
        {
            if (e.KeyCode == Keys.Delete)
            {
                XtraMessageBox.Show(this, "Vous ne pouvez pas supprimer la trace de passage!", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (e.KeyData == (Keys.Control | Keys.N))
            {
                barButtonItem1_ItemClick(null, null);
            }
        }
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) //OK
        {
            SetStatusBarContent(StatusBarEvents.Ready, "Prêt");
        }
        private void SetStatusBarContent(StatusBarEvents status, string valeur) // OK
        {
            if ((status == StatusBarEvents.Delete) || (status == StatusBarEvents.Ready) || (status == StatusBarEvents.Update))
            {
                this.StatusInfo.Caption = valeur;
            }
        }
        private void Principale_FormClosing(object sender, FormClosingEventArgs e) // OK
        {
            _objetConnexion.Fermer();
        }
        private void initialiserTableFLUX() //OK
        {
            tableFLUX.OptionsSelection.EnableAppearanceFocusedRow = true;
            tableFLUX.Appearance.FocusedRow.BackColor = Color.FromArgb(255, 128, 0);
            tableFLUX.Appearance.SelectedRow.BackColor = Color.FromArgb(255, 128, 0);
            tableFLUX.Appearance.SelectedRow.Options.UseBackColor = true;
        }
        private void lier() //OK
        {
            BindingSource _bind = new BindingSource();
            _bind.DataSource = _listeDonneesTable;
            controleTableau.DataSource = _bind;
        }

        public enum StatusBarEvents
        {
            Ready,
            Delete,
            Update
        }; // ???? tsay loatra

        private void statistiqueItem_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            // Statistique
            StatistiquePortail _stat = new StatistiquePortail(_objetConnexion.Interface);
            _stat.ShowDialog();

        }
        private void exportRapportItem_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //Rapport
            RapportEventForm rapport = new RapportEventForm(_objetConnexion.Interface);
            rapport.Show();
        }



        /// Ts miasa ty ato
        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Exporter...
            ExportationUtilisateur export = new ExportationUtilisateur();
            export.ShowDialog();
        }

    }
}
