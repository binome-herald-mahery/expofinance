﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraSplashScreen;




using InterfaceComms;

namespace CLIENT.Forms
{
    public partial class RapportEventForm : DevExpress.XtraEditors.XtraForm
    {
        private string _titreExportation = "Rapport des affluences"; // Peut-être changé...
        private List<InfoFluxClient> _liste;
        private string[] _listeInfoFlux = { "Date", "Code-Barre", "Nom", "Fonction", "Badge", "Pays", "Type de flux" }; 
        // Liste de ce qui est affiché dans InfoFluxClient... à changer évidemment si on change InfoFluxClient

        private IServerComm _connexionServeur;
        public RapportEventForm(IServerComm objConn_)
        {
            Cursor __current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            _connexionServeur = objConn_;

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
            Application.DoEvents();


            InitializeComponent();

            _liste = new List<InfoFluxClient>();
            lier();


            dateDebut.Properties.Mask.EditMask = "dd/MM/yyyy";
            dateDebut.Properties.Mask.UseMaskAsDisplayFormat = true;
            dateDebut.DateTime = DateTime.Now;

            dateFin.Properties.Mask.EditMask = "dd/MM/yyyy";
            dateFin.Properties.Mask.UseMaskAsDisplayFormat = true;
            dateFin.DateTime = DateTime.Now.AddMonths(2);


            saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
            saveFileDialog1.Title = "Exporter en format CSV";
            //this is the default behaviour
            saveFileDialog1.CheckPathExists = true;


            Cursor.Current = __current;
        }

        public void lier()
        {
            BindingSource __bind = new BindingSource();
            __bind.DataSource = _liste;
            gridControl1.DataSource = __bind;
        }

        private void LoadData()
        {
            try
            {
                // Remplir tableau
                _liste = _connexionServeur.ListePassage2("TOUS", -1,new DateTime(dateDebut.DateTime.Year, dateDebut.DateTime.Month, dateDebut.DateTime.Day,0,0,0), new DateTime(dateDebut.DateTime.Year, dateDebut.DateTime.Month, dateDebut.DateTime.Day, 23, 59, 59));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void RapportEvent_Load(object sender, EventArgs e)
        {
            LoadData();
            lier();
        }





        private void btnPrint_Click(object sender, EventArgs e)
        {
            // Check whether the GridControl can be previewed.
            if (!gridControl1.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Open the Preview window.
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            Cursor __current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            Application.DoEvents();

            //gridControl1.ShowPrintPreview();

            // Create objects and define event handlers.
            CompositeLink composLink = new CompositeLink(new PrintingSystem());
            //composLink.CreateMarginalHeaderArea += new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            PrintableComponentLink pcLink1 = new PrintableComponentLink();

            //Link linkMainReport = new Link();
            //linkMainReport.CreateDetailArea += new CreateAreaEventHandler(linkMainReport_CreateDetailArea);

            Link linkGrid1Report = new Link();
            linkGrid1Report.CreateDetailArea += new CreateAreaEventHandler(linkGrid1Report_CreateDetailArea);

            // Assign the controls to the printing links.
            pcLink1.Component = this.gridControl1;

            // Populate the collection of links in the composite link.
            // The order of operations corresponds to the document structure.
            composLink.Links.Add(linkGrid1Report);
            composLink.Links.Add(pcLink1);
            //composLink.Links.Add(linkMainReport);

            composLink.Margins = new System.Drawing.Printing.Margins(5, 5, 10, 10);

            // Create the report and show the preview window.
            composLink.ShowPreviewDialog();

            Cursor.Current = __current;
        }

        // Inserts a PageInfoBrick into the top margin to display the time.
        void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            e.Graph.DrawPageInfo(PageInfo.DateTime, "{0:hhhh:mmmm:ssss}", Color.Black,
                new RectangleF(0, 0, 200, 50), BorderSide.None);
        }

        // Creates a text header for the first grid.
        void linkGrid1Report_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {

            TextBrick tb = new TextBrick();
            tb.Text = "Rapport : Affluence ";
            tb.Font = new Font("Arial", 30);
            tb.ForeColor = Color.FromArgb(15, 157, 232);
            tb.Rect = new RectangleF(10, 0, 500, 60);
            tb.BorderWidth = 0;
            tb.BackColor = Color.Transparent;
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;
            e.Graph.DrawBrick(tb);
        }

        private void chkBetween_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBetween.Checked)
            {
                chkNow.Checked = false;
                dateDebut.Enabled = true;
                dateFin.Enabled = true;
            }
        }

        private void chkNow_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNow.Checked)
            {
                chkBetween.Checked = false;
                dateDebut.DateTime = DateTime.Now;
                dateFin.DateTime = DateTime.Now;
                dateDebut.Enabled = false;
                dateFin.Enabled = false;
            }
        }

        private void btnActualiser_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

            Cursor __current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            Application.DoEvents();

            LoadData();
            lier();

            Cursor.Current = __current;
        }

        private void btnExporter_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                exporterCSV(saveFileDialog1.FileName) ;
            }
        }

        private void exporterCSV(string path_)
        {
            if (path_.Trim().Length <= 1)
            {
                XtraMessageBox.Show("Veuillez sélectionner le fichier de destination.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try {
                CultureInfo __culture = CultureInfo.CurrentCulture;
                String __separateur = "";
                if (__culture.Name == "en-US")
                    __separateur = ",";
                else // if
                    __separateur = ";";

                StringBuilder __csv = new StringBuilder();

                __csv.AppendLine(_titreExportation);
                __csv.AppendLine("");
                // Header....
                for (int i = 0; i < _listeInfoFlux.Length - 1; i++) // Oui, c'est bien Length-1...
                {
                    __csv.Append(_listeInfoFlux[i] + __separateur);
                }
                __csv.AppendLine(_listeInfoFlux[_listeInfoFlux.Length - 1]);


                foreach (InfoFluxClient flux in _liste)
                {
                    string newLine = string.Format("{0};{1};{2};{3};{4};{5};{6}", flux.Date, flux.CodeBarre, flux.Nom, flux.Fonction, flux.TypeEtNuméro, flux.Pays, flux.TypeDeFlux);
                    __csv.AppendLine(newLine);
                    newLine = null;
                }

                File.WriteAllText(path_, __csv.ToString(),System.Text.Encoding.GetEncoding("iso-8859-15"));

                XtraMessageBox.Show("Données exportées avec succès.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } catch
            {
                XtraMessageBox.Show("Erreur d'exportation des données.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
