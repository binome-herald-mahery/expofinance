﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using InterfaceComms;

namespace CLIENT.Forms
{
    public partial class ExportationUtilisateur : DevExpress.XtraEditors.XtraForm
    {
        public ExportationUtilisateur()
        {
            InitializeComponent();

            saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
            saveFileDialog1.Title = "Exporter en format CSV";
            //this is the default behaviour
            saveFileDialog1.CheckPathExists = true;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textFile.Text = saveFileDialog1.FileName;
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //if (textFile.Text.Trim().Length > 1)
            //{
            //    CultureInfo __culture = CultureInfo.CurrentCulture;
            //    String __separateur = "";
            //    if (__culture.Name == "en-US")
            //        __separateur = ",";
            //    else // if
            //        __separateur = ";";

            //    StringBuilder csv = new StringBuilder();

            //    List<InfoFluxClient> visiteurData = GetAllVisiteur();

            //    csv.AppendLine("Numero Badge;Nom;Prenoms;Societe");

            //    foreach (VisiteurInfo visiteur in visiteurData)
            //    {
            //        string newLine = string.Format("{0};{1};{2};{3}", visiteur.Code, visiteur.Nom, visiteur.Prenom, visiteur.Societe);
            //        csv.AppendLine(newLine);
            //        newLine = null;
            //    }

            //    File.WriteAllText(textFile.Text, csv.ToString());

            //    XtraMessageBox.Show("Données exportées avec succès.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //else
            //{
            //    XtraMessageBox.Show("Veuillez sélectionner le fichier de destination.", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}