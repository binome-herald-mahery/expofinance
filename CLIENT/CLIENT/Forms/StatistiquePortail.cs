﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using System.IO;

using InterfaceComms;

namespace CLIENT.Forms
{
    public partial class StatistiquePortail : DevExpress.XtraEditors.XtraForm
    {
        public struct DataVisiteur
        {
            public int hr6_7;
            public int hr7_8;
            public int hr8_9;
            public int hr9_10;
            public int hr10_11;
            public int hr11_12;
            public int hr12_13;
            public int hr13_14;
            public int hr14_15;
            public int hr15_16;
            public int hr16_17;
            public int hr17_18;
            public int hr18_19;
            public int hr19_20;
        };

        public struct DataExposant
        {
            public int hr6_7;
            public int hr7_8;
            public int hr8_9;
            public int hr9_10;
            public int hr10_11;
            public int hr11_12;
            public int hr12_13;
            public int hr13_14;
            public int hr14_15;
            public int hr15_16;
            public int hr16_17;
            public int hr17_18;
            public int hr18_19;
            public int hr19_20;
        };

        private IServerComm _objetConnexion;

        public StatistiquePortail(IServerComm objetConnexion_)
        {
            Application.DoEvents();

            InitializeComponent();

            _objetConnexion = objetConnexion_;
        }


        private void Statistique_Load(object sender, EventArgs e)
        {
            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }



            remplirGraphe();


            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null)
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();



            // Cast the chart's diagram to the XYDiagram type, to access its axes.
            XYDiagram diagram = statistiqueGraph.Diagram as XYDiagram;

            // Define the detail level for date-time values.
            diagram.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Minute;

            // Define the date-time measurement unit, to which the beginning of 
            // a diagram's gridlines and labels should be aligned.
            diagram.AxisX.DateTimeScaleOptions.GridAlignment = DateTimeGridAlignment.Hour;


            // Define the custom date-time format (name of a month) for the axis labels.
            //diagram.AxisX.Label.TextPattern = "{V:dd/MM HH:mm}";

            // Add both series to the chart.
            //fullStackedLineChart.Series.AddRange(new Series[] { series1, series2 });

            // Access the type-specific options of the diagram.
            diagram.EnableAxisXZooming = true;
            //diagram.AxisY.Label.TextPattern = "{VP:P0}";

            statistiqueGraph.Legend.Visible = true;
        }

        private void remplirGraphe()
        {
            Cursor __currentCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            DataVisiteur dataAll = GetData();

            var points = statistiqueGraph.Series[0].Points;
            List<SeriesPoint> tempPoints = new List<SeriesPoint>();
            foreach (SeriesPoint point in statistiqueGraph.Series[0].Points) tempPoints.Add(point);
            foreach (SeriesPoint point in tempPoints) statistiqueGraph.Series[0].Points.Remove(point);

            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0), dataAll.hr6_7));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0), dataAll.hr7_8));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0), dataAll.hr8_9));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0), dataAll.hr9_10));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 0, 0), dataAll.hr10_11));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0), dataAll.hr11_12));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0), dataAll.hr12_13));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0), dataAll.hr13_14));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 0, 0), dataAll.hr14_15));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 16, 0, 0), dataAll.hr15_16));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0), dataAll.hr16_17));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0), dataAll.hr17_18));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0), dataAll.hr18_19));
            statistiqueGraph.Series[0].Points.Add(new SeriesPoint(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20, 0, 0), dataAll.hr19_20));

            Cursor.Current = __currentCursor ;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            remplirGraphe();
        }

        private void Statistique_Shown(object sender, EventArgs e)
        {
            timer1.Start();
        }

        void ShowMenu(ChartHitInfo hi, Point p)
        {
            DevExpress.XtraBars.PopupMenu menu = new DevExpress.XtraBars.PopupMenu(barManager1);
            menu.ShowCaption = true;
            menu.MenuCaption = "Statistique des affluences";


            DevExpress.XtraBars.BarButtonItem item1 = new DevExpress.XtraBars.BarButtonItem() { Caption = "Exporter en image" };
            item1.Id = 1;
            item1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Click);

            DevExpress.XtraBars.BarButtonItem item2 = new DevExpress.XtraBars.BarButtonItem() { Caption = "Exporter en PDF" };
            item2.Id = 2;
            item2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Click);

            DevExpress.XtraBars.BarButtonItem item3 = new DevExpress.XtraBars.BarButtonItem() { Caption = "Print" };
            item3.Id = 3;
            item3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Click);

            DevExpress.XtraBars.BarButtonItem item4 = new DevExpress.XtraBars.BarButtonItem() { Caption = "Actualiser" };
            item4.Id = 4;
            item4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Click);

            menu.AddItems(new DevExpress.XtraBars.BarButtonItem[] { item1, item2, /*item3,*/ item4 });

            menu.ShowPopup(statistiqueGraph.PointToScreen(p));

        }

        private void barButtonItem_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e.Item.Id == 1)
            {
                // Export en Image
                if (statistiqueGraph.IsPrintingAvailable)
                {
                    statistiqueGraph.ExportToImage(@"..\STATISTIQUE\StatGraphique.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    MessageBox.Show("Exportation effectuée");
                }
            }
            else if (e.Item.Id == 2)
            {
                // Export en PDF
                if (statistiqueGraph.IsPrintingAvailable)
                {
                    string dstFilename = @"..\STATISTIQUE\StatGraphique" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm") + ".pdf";

                    string subPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\TempData\";


                    if (!System.IO.Directory.Exists(subPath))
                    {
                        try
                        {
                            System.IO.Directory.CreateDirectory(subPath);
                        }
                        catch (IOException ex) { Console.WriteLine(ex.Message); return; }
                    }


                    string srcFilename = subPath + @"\File-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm") + ".jpg";
                    iTextSharp.text.Rectangle pageSize = null;

                    using (var ms = new MemoryStream())
                    {
                        statistiqueGraph.ExportToImage(srcFilename, System.Drawing.Imaging.ImageFormat.Jpeg);

                        var srcImage = new Bitmap(srcFilename);
                        pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);

                        var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 0, 0, 30f, 0);

                        iTextSharp.text.pdf.PdfWriter.GetInstance(document, ms).SetFullCompression();
                        document.Open();
                        var image = iTextSharp.text.Image.GetInstance(srcFilename);
                        image.ScaleToFit(document.PageSize.Width, document.PageSize.Height);
                        document.Add(image);
                        document.Close();
                        File.WriteAllBytes(dstFilename, ms.ToArray());
                        srcImage.Dispose(); // Disposing resource for Bitmap
                        document.Dispose(); // Disposing resource for Document
                    }

                    // Suppressions des fichiers temporaires
                    if (File.Exists(dstFilename)) System.Diagnostics.Process.Start(dstFilename);
                    if (File.Exists(srcFilename))
                    {
                        try
                        {
                            File.Delete(srcFilename);
                        }
                        catch (IOException ex) { Console.WriteLine(ex.Message); }
                    }
                }
            }
            else if (e.Item.Id == 3)
            {
                //string s = "^XA^LH30,30\n^FO20,10^ADN,90,50^AD^FDHello World^FS\n^XZ";

                //// Allow the user to select a printer.
                //PrintDialog pd = new PrintDialog();
                //pd.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
                //if (DialogResult.OK == pd.ShowDialog(this))
                //{
                //    // Send a printer-specific to the printer.
                //    //RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, s);

                //}
            }
            else
            {
                remplirGraphe();
            }
        }

        private DataVisiteur GetData()
        {
            DataVisiteur retval = new DataVisiteur();

            try
            {
                List<int> __liste = _objetConnexion.NombreFlux(DateTime.Now);
                retval.hr6_7 = __liste[0];
                retval.hr7_8 = __liste[1];
                retval.hr8_9 = __liste[2];
                retval.hr9_10 = __liste[3]; 
                retval.hr10_11 = __liste[4];
                retval.hr11_12 = __liste[5];
                retval.hr12_13 = __liste[6];
                retval.hr13_14 = __liste[7];
                retval.hr14_15 = __liste[8];
                retval.hr15_16 = __liste[9];
                retval.hr16_17 = __liste[10];
                retval.hr17_18 = __liste[11];
                retval.hr18_19 = __liste[12];
                retval.hr19_20 = __liste[13];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return retval;
        }

        private void statistiqueGraph_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ShowMenu(statistiqueGraph.CalcHitInfo(e.Location), e.Location);
            }
        }

        private void Statistique_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            timer1.Dispose();
        }
    }
}