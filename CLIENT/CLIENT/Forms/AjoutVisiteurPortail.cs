﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.IO;

using InterfaceComms;

namespace CLIENT.Forms
{
    public partial class AjoutVisiteurPortail : Form
    {
        public delegate void EvClickAjoutVisiteur(VisiteurPassage Resultat, bool insertion) ; // insertion = true si on va inserer, false si on va fermer
        public event EvClickAjoutVisiteur Action ;


        private int _maxTextLength = 8; // On peut changer ceci au cas où...
        private char[] _listeToucheAccepté = 
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        }; // On met ici la liste des touches acceptées comme code. 
        // ET QUE L'ON N'ACCEPTE PLUS LORSQUE LA LONGUEUR MAXIMUM DU TEXTE EST ATTEINTE....

        private char[] _listeToucheSupplementaire =
        {
            '\b' // "BACKSPACE"
        }; // Liste des autres touches acceptées même si la longueur maximale est atteinte.
           // Il est très peu probable que l'on doive ajouter un élément dans cette liste. 
           // Toutefois, on peut facilement le faire si jamais on en aura besoin


        private Color CouleurNONEmphase { get { return Color.LightGray; } }
        private Color CouleurEmphase { get { return Color.Yellow; } }


        public void ConsoleLog(string texte, bool emphase = true) // OK
        {
            this.txtLog.Text = texte ;
            this.txtLog.BackColor = emphase?CouleurEmphase:CouleurNONEmphase;
        }

        public AjoutVisiteurPortail(char type_par_defaut)
        {
            InitializeComponent();

            ConsoleLog("Forme initialisée");
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;

            if (type_par_defaut == 'E')
                this.radioTypeFlux.SelectedIndex = 0 ;
            else if (type_par_defaut == 'S') // On n'est jamais trop prudent....
                this.radioTypeFlux.SelectedIndex = 1; 

            this.textNumBadge.Focus();
        }


        private bool IsValidData()
        {
            int myNumber;
            return !
                (
                    ((textNumBadge.Text.Length > 0)&&!int.TryParse(textNumBadge.Text, out myNumber)) // Si ce n'est pas un nombre
                    ||
                    (textNumBadge.Text.Trim().Length != _maxTextLength) // si la longeur ne correspond pas
                    ||
                    (radioTypeFlux.EditValue == null) // Si le choix du type de portail est vide!
                );
        }


        private void btnEnregistrer_Click(object sender, EventArgs e) // A voir de près...
        {
            if (IsValidData())
            {
                VisiteurPassage visiteur = new VisiteurPassage();
                visiteur.NumBadge = Convert.ToInt32(textNumBadge.Text); // On a besoin de seulement ces trois lignes (Voir: Principale.ActionAjoutVisiteur)
                visiteur.TypeFlux = radioTypeFlux.EditValue.ToString();
                visiteur.DatePassage = DateTime.Now;

                textNumBadge.Text = "";

                Action(visiteur,true);

            }
            else
            {
                textNumBadge.Text = "";
                XtraMessageBox.Show("Veuillez remplir les champs obligatoire!\n-Il faut entrer un nombre de " + _maxTextLength + " caractères\n-Il faut choisir le type de flux (Entrée ou Sortie)", "Gestion Visiteur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e) // OK
        {
            Action(null, false);
        }

        private void textNumBadge_KeyDown(object sender, KeyEventArgs e) // OK
        {
            this.txtLog.BackColor = CouleurNONEmphase ;
            
            AddUserPortail_KeyDown(sender, e);
        }

        private void AddUserPortail_FormClosing(object sender, FormClosingEventArgs e) // OK
        {
            Action(null, false);
        }


        internal void EffacerZoneTexte() // OK
        {
            this.textNumBadge.Text = null ;
        }

        internal void ReFocus() // OK
        {
            this.textNumBadge.Focus();
        }


        private void AddUserPortail_KeyDown(object sender, KeyEventArgs e) // OK
        {
            if (e.KeyCode == Keys.Enter)
            {

                btnEnregistrer_Click(sender,e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btnAnnuler_Click(sender, e);
            }
            else
            {
                if (this.textNumBadge.Text.Length >= _maxTextLength)
                {
                    // On bloque l'action des touches, sauf pour "Left, Right, Entrer" et "backspace" qui est automatique, 
                    // car l'appui sur ce dernier n'invoque pas cette méthode
                    if ((e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right) && (e.KeyCode != Keys.Enter))
                        e.Handled = true;
                }
            }
        }

        private void textNumBadge_KeyPress(object sender, KeyPressEventArgs e) // OK
        {
            // On n'accepte que les touches dans la liste des touches acceptées.
            bool __clear1 = false ;
            bool __clear2 = false ;
            foreach (char touche in _listeToucheAccepté)
            {
                if (e.KeyChar == touche)
                {
                    __clear1 = true;
                    break;
                }
            }
            if (!__clear1)
                foreach (char touche in _listeToucheSupplementaire)
                {
                    if (e.KeyChar == touche)
                    {
                        __clear2 = true;
                        break;
                    }
                }


            if (!(__clear1 || __clear2))
            {
                e.Handled = true; // On n'accepte pas cette touche
                return;
            }

            if ((textNumBadge.Text.Length >= _maxTextLength) && (true))
            {
                if (__clear1) e.Handled = true;
                else if (!__clear2) e.Handled = true;
            }
        }

    }
}
