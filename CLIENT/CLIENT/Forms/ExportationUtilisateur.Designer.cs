﻿namespace CLIENT.Forms
{
    partial class ExportationUtilisateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportationUtilisateur));
            this.btnAnnuler = new DevExpress.XtraEditors.SimpleButton();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.btnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.textFile = new DevExpress.XtraEditors.TextEdit();
            this.labelDestinationFile = new DevExpress.XtraEditors.LabelControl();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.textFile.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuler.Location = new System.Drawing.Point(122, 50);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(96, 45);
            this.btnAnnuler.TabIndex = 9;
            this.btnAnnuler.Text = "Fermer";
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(279, 50);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(94, 45);
            this.btnExport.TabIndex = 8;
            this.btnExport.Text = "Exporter";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(447, 7);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 7;
            this.btnBrowse.Text = "Parcourir...";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // textFile
            // 
            this.textFile.Location = new System.Drawing.Point(122, 9);
            this.textFile.Name = "textFile";
            this.textFile.Size = new System.Drawing.Size(301, 20);
            this.textFile.TabIndex = 6;
            // 
            // labelDestinationFile
            // 
            this.labelDestinationFile.Location = new System.Drawing.Point(12, 12);
            this.labelDestinationFile.Name = "labelDestinationFile";
            this.labelDestinationFile.Size = new System.Drawing.Size(94, 13);
            this.labelDestinationFile.TabIndex = 5;
            this.labelDestinationFile.Text = "Fichier destination :";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // ExportationUtilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 109);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.textFile);
            this.Controls.Add(this.labelDestinationFile);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportationUtilisateur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exporter participant";
            ((System.ComponentModel.ISupportInitialize)(this.textFile.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnAnnuler;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraEditors.SimpleButton btnBrowse;
        private DevExpress.XtraEditors.TextEdit textFile;
        private DevExpress.XtraEditors.LabelControl labelDestinationFile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}