﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CLIENT.Forms
{
    public partial class Message : DevExpress.XtraEditors.XtraForm
    {
        public Message(string messageErreur_)
        {
            InitializeComponent();
            label.Text = messageErreur_;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Dispose(true);
        }
    }
}