﻿using DevExpress.LookAndFeel ;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using CLIENT.Forms;

namespace CLIENT
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
//            bool _clear = true ;
            // Initialisation VS
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            // Initialisations relatives à DevExpress
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
            UserLookAndFeel.Default.SkinName = "Liquid Sky" ;
            UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin ;

            if (DevExpress.XtraSplashScreen.SplashScreenManager.Default != null) // Sparkline
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

/** ??? **/

/** /??? **/

            Application.Run(new Principale());
        }
    }
}
